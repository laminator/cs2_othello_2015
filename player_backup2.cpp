/*
 * @filename player_backup2.cpp
 */

/* Header Files */
#include <vector>
#include "player.h"

/* Definitions */
#define INVALID -9999

/* Namespaces */
using namespace std;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side givenSide) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
     
     currentBoard = Board();
     side = givenSide;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * TODO
 */
Move *Player::minimax() {
    
    /* Stores player's possible moves at the first level */
    vector<Move*> moves_levelOne;
    
    /* Initialize variables */
    Move *try_move = new Move(0, 0);
    
    /* Iterate through all valid moves. */
    for (int i = 0; i < 8; i++) {
        try_move->setY(i);
        for (int j = 0; j < 8; j++) {
            try_move->setX(j);
            if (currentBoard.checkMove(try_move, side)) {
                moves_levelOne.push_back(new Move(j, i));
            }
        }
    }
    
    /* Compute opponent's valid moves. */
    
    // Initialize variables
    int num_moves = moves_levelOne.size();
    Side opp_side = (side == BLACK) ? WHITE : BLACK;
    vector< vector<Move*>* > opp_levelOne;
    
    // Stores moves to try
    Board *nextBoard;
    
    // Generate and store opponent's valid moves
    for (int x = 0; x < num_moves; x++) {
        nextBoard = currentBoard.copy();
        nextBoard->doMove(moves_levelOne[x], side);
        opp_levelOne.push_back(new vector<Move*>);
        for (int i = 0; i < 8; i++) {
            try_move->setY(i);
            for (int j = 0; j < 8; j++) {
                try_move->setX(j);
                if (nextBoard->checkMove(try_move, opp_side)) {
                    opp_levelOne[x]->push_back(new Move(j, i));
                }
            }
        }
        delete nextBoard;
    }
    
    /* Compute reponse to opponent's moves and score them. */
    
    Board *nextNextBoard;       // lol
    int best_score;             // such efficient
    int temp_score;             // very programmer
    Move *best_move;            // much design
    
    // It's under 9,000!
    best_move = new Move(0, 0);
    best_score = INVALID;   
    
    // For every possible initial move...
    for (int x = 0; x < num_moves; x++) {
    
        // For every possible opponent's response to the initial move...
        for (unsigned int y = 0; y < opp_levelOne[x]->size(); y++) {
        
            // Simulate the first two moves
            nextBoard = currentBoard.copy();
            nextBoard->doMove(moves_levelOne[x], side);
            nextBoard->doMove((*opp_levelOne[x])[y], opp_side);
        
            // Try and, if applicable, score the current player's second move
            for (int i = 0; i < 8; i++) {
                nextNextBoard = nextBoard->copy();
                try_move->setY(i);
                for (int j = 0; j < 8; j++) {
                    try_move->setX(j);
                    if (nextNextBoard->checkMove(try_move, side)) {
                        nextNextBoard->doMove(try_move, side);
                        temp_score = nextNextBoard->count(side) - nextNextBoard->count(opp_side);
                        if (temp_score >= best_score) {
                            best_move->setY(moves_levelOne[x]->getY());
                            best_move->setX(moves_levelOne[x]->getX());
                            best_score = temp_score;
                        }
                        delete nextNextBoard;
                        nextNextBoard = nextBoard->copy();
                    }
                }
                delete nextNextBoard;
            }
            
            // Delete 'nextBoard'
            delete nextBoard;
        
        }
        
    }
    
    /* Clear memory. */
    
    delete try_move;
    
    for (unsigned int i = 0; i < moves_levelOne.size(); i++) {
        delete moves_levelOne[i];
    }
    
    for (unsigned int i = 0; i < opp_levelOne.size(); i++) {
        for (unsigned int j = 0; j < opp_levelOne[i]->size(); j++) {
            delete (*opp_levelOne[i])[j];
        }
        delete opp_levelOne[i];
    }
    
    /* Apply the best move to the current board. */
    currentBoard.doMove(best_move, side);
    
    /* Return the best move. */
    return best_move;

}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    /* Store the next board. */
    Board *nextBoard;
    
    /* Store the side. */
    Side try_side;
    
    /* Process the opponent's move */
    try_side = (side == BLACK) ? WHITE : BLACK;
    currentBoard.doMove(opponentsMove, try_side);
    
    /* Test minimax() here. */
    if (testingMinimax) {
        return minimax();
    }
    
    /* If we have no moves left, return NULL. Otherwise, do a move. */
    if (!currentBoard.hasMoves(side)) {
        return NULL;
    } else {
        
        // Initialize the move
        Move *try_move = new Move(0, 0);
        try_side = side;
        
        // Iterate through all valid moves. Score and store the valid moves.
        int coords[8][8];
        for (int i = 0; i < 8; i++) {
            try_move->setY(i);
            for (int j = 0; j < 8; j++) {
                nextBoard = currentBoard.copy();
                try_move->setX(j);
                if (currentBoard.checkMove(try_move, try_side)) {
                    nextBoard->doMove(try_move, try_side);
                    coords[i][j] = nextBoard->count(try_side);
                } else {
                    coords[i][j] = INVALID;
                }
                delete nextBoard;   
            }
        }
        
        /* Find the best valid move. */ 
        
        int high_i = 0;
        int high_j = 0;
        int high_score = 0;
        
        // Find the highest-scoring corner, if possible.
        bool corner_taken = false;
        if (coords[0][0] != INVALID) {
            high_score = coords[0][0];
            corner_taken = true;
        }
        if ((coords[0][8 - 1] != INVALID) && (coords[0][8 - 1] >= high_score)) {
            high_i = 0;
            high_j = 8 - 1;
            high_score = coords[0][8 - 1];
            corner_taken = true;
        }
        if ((coords[8 - 1][0] != INVALID) && (coords[8 - 1][0] >= high_score)) {
            high_i = 8 - 1;
            high_j = 0;
            high_score = coords[8 - 1][0];
            corner_taken = true;
        }
        if ((coords[8 - 1][8 - 1] != INVALID) && (coords[8 - 1][8 - 1] >= high_score)) {
            high_i = 8 - 1;
            high_j = 8 - 1;
            high_score = coords[8 - 1][8 - 1];
            corner_taken = true;
        }
        
        // Find the highest-scoring non-corner move, if applicable
        if (!corner_taken) {
            high_i = 0;
            high_j = 0;
            high_score = 0;
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if ((coords[i][j] != INVALID) && (coords[i][j] >= high_score)) {
                        high_i = i;
                        high_j = j;
                        high_score = coords[i][j];
                    }
                }  
            }
        }
        
        // Set the highest-scoring move
        try_move->setY(high_i);
        try_move->setX(high_j);
        
        // Apply the move to the current board
        currentBoard.doMove(try_move, side);
        
        // Return the highest-scoring move
        return try_move; 
        
    }

}
