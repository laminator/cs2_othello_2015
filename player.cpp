/*
 * @filename player.cpp
 */

/* Header Files */
#include <stdio.h>
#include <vector>
#include "player.h"

/* Definitions */
#define INVALID            -10
#define NO_OPP_MOVE         9001

// Corner and edge weights (for certain strategies)
#define CORNER_WEIGHT       1000
#define EDGE_WEIGHT         50
#define OPP_CORNER_WEIGHT   0.001
#define OPP_EDGE_WEIGHT     0.02

/* Namespaces */
using namespace std;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side givenSide) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
     
    currentBoard = Board();
    side = givenSide;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * @brief   Implements a two-deep minimax decision tree to compute the best  
 *          next move for the player.
 *
 * @return  A pointer to the best next move as computed by a two-deep 
 *          minimax decision tree.
 *
 */
Move *Player::minimax() {
    
    /* Stores player's possible moves at the first level */
    vector<Move*> moves_levelOne;
    
    /* Initialize variables */
    Move *try_move = new Move(0, 0);
    
    /* Iterate through all valid moves. */
    for (int i = 0; i < 8; i++) {
        try_move->setY(i);
        for (int j = 0; j < 8; j++) {
            try_move->setX(j);
            if (currentBoard.checkMove(try_move, side)) {
                moves_levelOne.push_back(new Move(j, i));
            }
        }
    }
    
    /* DEBUGGING: Print out the list of possible moves */
    fprintf(stderr, "Possible moves:\n");
    for (unsigned int i = 0; i < moves_levelOne.size(); i++) {
        fprintf(stderr, "(%d, %d)\n", moves_levelOne[i]->getX(), moves_levelOne[i]->getY());
    }
    
    /* Compute and score opponent's valid moves. */
    
    // Initialize variables
    int num_moves = moves_levelOne.size();
    Side opp_side = (side == BLACK) ? WHITE : BLACK;
    
    // Stores moves to try
    Board *nextBoard;
    
    // Used to score opponent's moves
    Board *nextNextBoard;
    int *worst_scores = new int[num_moves];
    int temp_score;
        
    // It's over 9,000!
    for (int x = 0; x < num_moves; x++) {
      worst_scores[x] = NO_OPP_MOVE;  
    }
    
    // Generate and store opponent's valid moves
    for (int x = 0; x < num_moves; x++) {
        nextBoard = currentBoard.copy();
        nextBoard->doMove(moves_levelOne[x], side);
        for (int i = 0; i < 8; i++) {
            try_move->setY(i);
            for (int j = 0; j < 8; j++) {
                try_move->setX(j);
                if (nextBoard->checkMove(try_move, opp_side)) {
                    nextNextBoard = nextBoard->copy();
                    nextNextBoard->doMove(try_move, opp_side);
                    temp_score = nextNextBoard->count(side) - nextNextBoard->count(opp_side);
                    if (temp_score < worst_scores[x]) {
                        worst_scores[x] = temp_score;
                    }
                    delete nextNextBoard;
                }
            }
        }
        delete nextBoard;
    } 

    /* DEBUGGING: Print out the list of worst scores */
    fprintf(stderr, "Worst scores:\n");
    for (int i = 0; i < num_moves; i++) {
        fprintf(stderr, "%d\n", worst_scores[i]);
    }

    /* Find the best move. */
    
    // Stores the best move
    Move *best_move = new Move(moves_levelOne[0]->getX(), moves_levelOne[0]->getY());
    
    // Initialize weights
    /*
    double weights[8][8] = {{320, 0.2,  40,   5,   5,  40, 0.2, 320},
                            {0.2, 0.1, 0.5, 0.5, 0.5, 0.5, 0.1, 0.2},
                            { 40, 0.5,  15,   1,   1,  15, 0.5,  40},
                            {  5, 0.5,   1,   1,   1,   1, 0.5,   5},
                            {  5, 0.5,   1,   1,   1,   3, 0.5,   5},
                            { 40, 0.5,  15,   1,   1,  15, 0.5,  40},
                            {0.2, 0.1, 0.5, 0.5, 0.5, 0.5, 0.1, 0.2},
                            {320, 0.2,  40,   5,   5,  40, 0.2, 320}};
    */
    
    
    // Identify the move that maximizes our minimum gain
    temp_score = worst_scores[0];
    for (int x = 0; x < num_moves; x++) {
        /*
        if (worst_scores[x] != NO_OPP_MOVE) {
            worst_scores[x] *= weights[moves_levelOne[x]->getY()][moves_levelOne[x]->getX()];
            if (worst_scores[x] > temp_score) {
                best_move->setX(moves_levelOne[x]->getX());
                best_move->setY(moves_levelOne[x]->getY());
                temp_score = worst_scores[x];
            }
        }
        */
        if ((worst_scores[x] != NO_OPP_MOVE) && (worst_scores[x] > temp_score)) {
            best_move->setX(moves_levelOne[x]->getX());
            best_move->setY(moves_levelOne[x]->getY());
            temp_score = worst_scores[x];
        }
    }
    
    /* Clear memory. */
    for (unsigned int i = 0; i < moves_levelOne.size(); i++) {
        delete moves_levelOne[i];
    }
    delete try_move;
    delete worst_scores;
    
    /* Apply the best move to the current board. */
    currentBoard.doMove(best_move, side);
    
    /* Return the best move. */
    return best_move;

}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    /* Store the next board. */
    Board *nextBoard;
    
    /* Store the side. */
    Side try_side;
    
    /* Process the opponent's move */
    try_side = (side == BLACK) ? WHITE : BLACK;
    currentBoard.doMove(opponentsMove, try_side);
    
    /* If we have no moves left, return NULL. Otherwise, do a move. */
    if (!currentBoard.hasMoves(side)) {
        return NULL;
    } else if (testingMinimax) {    // Use a minimax decision tree if specified
        return minimax();
    } else {
        
        // Initialize the move
        Move *try_move = new Move(0, 0);
        try_side = side;
        
        // Iterate through all valid moves. Score and store the valid moves.
        double coords[8][8];
        for (int i = 0; i < 8; i++) {
            try_move->setY(i);
            for (int j = 0; j < 8; j++) {
                nextBoard = currentBoard.copy();
                try_move->setX(j);
                if (currentBoard.checkMove(try_move, try_side)) {
                    nextBoard->doMove(try_move, try_side);
                    coords[i][j] = nextBoard->count(try_side);
                } else {
                    coords[i][j] = INVALID;
                }
                delete nextBoard;   
            }
        }
        
        /* Find the best valid move. */ 
        
        // Initialize weights
        double weights[8][8] = {{320, 0.2,  40,   5,   5,  40, 0.2, 320},
                                {0.2, 0.1, 0.5, 0.5, 0.5, 0.5, 0.1, 0.2},
                                { 40, 0.5,  15,   1,   1,  15, 0.5,  40},
                                {  5, 0.5,   1,   1,   1,   1, 0.5,   5},
                                {  5, 0.5,   1,   1,   1,   3, 0.5,   5},
                                { 40, 0.5,  15,   1,   1,  15, 0.5,  40},
                                {0.2, 0.1, 0.5, 0.5, 0.5, 0.5, 0.1, 0.2},
                                {320, 0.2,  40,   5,   5,  40, 0.2, 320}};
        
        // Weigh the squares
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                coords[i][j] *= weights[i][j];
            }  
        }
        
        /*
        // Weigh the edges
        for (int i = 0; i < 8; i++) {
            coords[i][0]     *= EDGE_WEIGHT;
            coords[i][8 - 1] *= EDGE_WEIGHT;
            coords[0][i]     *= EDGE_WEIGHT;
            coords[8 - 1][i] *= EDGE_WEIGHT;
        }
        
        // Undo the edge weights on the corners
        coords[0][0]          /= (EDGE_WEIGHT * EDGE_WEIGHT);
        coords[8 - 1][0]      /= (EDGE_WEIGHT * EDGE_WEIGHT);
        coords[0][8 - 1]      /= (EDGE_WEIGHT * EDGE_WEIGHT);
        coords[8 - 1][8 - 1]  /= (EDGE_WEIGHT * EDGE_WEIGHT);
        
        // Weigh the corners
        coords[0][0]          *= CORNER_WEIGHT;
        coords[8 - 1][0]      *= CORNER_WEIGHT;
        coords[0][8 - 1]      *= CORNER_WEIGHT;
        coords[8 - 1][8 - 1]  *= CORNER_WEIGHT;
                
        // Weigh the opponent's edges
        for (int i = 1; i < (8 - 1); i++) {
            coords[i][1]     *= OPP_EDGE_WEIGHT;
            coords[i][8 - 2] *= OPP_EDGE_WEIGHT;
            coords[1][i]     *= OPP_EDGE_WEIGHT;
            coords[8 - 2][i] *= OPP_EDGE_WEIGHT;
        }
        
        // Undo the edge weights on the opponent's corners
        coords[1][1]          /= (OPP_EDGE_WEIGHT * OPP_EDGE_WEIGHT);
        coords[8 - 2][1]      /= (OPP_EDGE_WEIGHT * OPP_EDGE_WEIGHT);
        coords[1][8 - 2]      /= (OPP_EDGE_WEIGHT * OPP_EDGE_WEIGHT);
        coords[8 - 2][8 - 2]  /= (OPP_EDGE_WEIGHT * OPP_EDGE_WEIGHT);
        
        // Weigh the opponent's corners
        coords[1][1]          *= OPP_CORNER_WEIGHT;
        coords[8 - 2][1]      *= OPP_CORNER_WEIGHT;
        coords[1][8 - 2]      *= OPP_CORNER_WEIGHT;
        coords[8 - 2][8 - 2]  *= OPP_CORNER_WEIGHT;
        */
        
        // Find the highest-scoring move
        int high_i = 0;
        int high_j = 0;
        double high_score = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (coords[i][j] >= high_score) {
                    high_i = i;
                    high_j = j;
                    high_score = coords[i][j];
                }
            }  
        }
        
        // Find the highest-scoring corner, if possible.
        /*
        bool corner_taken = false;
        if (coords[0][0] != INVALID) {
            high_score = coords[0][0];
            corner_taken = true;
        }
        if ((coords[0][8 - 1] != INVALID) && (coords[0][8 - 1] >= high_score)) {
            high_i = 0;
            high_j = 8 - 1;
            high_score = coords[0][8 - 1];
            corner_taken = true;
        }
        if ((coords[8 - 1][0] != INVALID) && (coords[8 - 1][0] >= high_score)) {
            high_i = 8 - 1;
            high_j = 0;
            high_score = coords[8 - 1][0];
            corner_taken = true;
        }
        if ((coords[8 - 1][8 - 1] != INVALID) && (coords[8 - 1][8 - 1] >= high_score)) {
            high_i = 8 - 1;
            high_j = 8 - 1;
            high_score = coords[8 - 1][8 - 1];
            corner_taken = true;
        }
        */
        
        // Find the highest-scoring non-corner move, if applicable
        /*
        if (!corner_taken) {
            high_i = 0;
            high_j = 0;
            high_score = 0;
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if ((coords[i][j] != INVALID) && (coords[i][j] >= high_score)) {
                        high_i = i;
                        high_j = j;
                        high_score = coords[i][j];
                    }
                }  
            }
        }
        */
        
        // Set the highest-scoring move
        try_move->setY(high_i);
        try_move->setX(high_j);
        
        // Apply the move to the current board
        currentBoard.doMove(try_move, side);
        
        // Return the highest-scoring move
        return try_move; 
        
    }

}
