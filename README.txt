Hi! I worked by myself on this assignment.

Here's the progression of strategies I tried:

=======
-  1  -
=======

STRATEGY:

Pick the move that maximizes the player's count of pieces on the board after 
one move.

RESULTS:

Didn't even beat SimplePlayer. This makes sense because SimplePlayer will have 
a greater chance to randomly grab an edge or a corner as the board expands.

=======
-  2  -
=======

STRATEGY:

If a corner is available, take it. Otherwise, pick the move that maximizes the
player's count of pieces on the board after one move.

RESULTS:

This strategy was enough to beat SimplePlayer the vast majority of the time, 
and was the strategy I submitted for Assignment #9. It didn't do well against 
ConstantTimePlayer, however.

=======
-  3  -
=======

STRATEGY:

"Score" each available move as the count of your own pieces after that move.
Then, weigh certain squares with certain factors:

- Corners with CORNER_WEIGHT (>> 1)
- Edges with EDGE_WEIGHT (>> 1)
- The second-outermost edge with OPP_EDGE_WEIGHT (<< 1)
- The second-outermost corners with OPP_CORNER_WEIGHT (<< 1)

RESULTS:

This strategy was enough to beat ConstantTimePlayer about 60% of the 
time, depending on how I tweaked the weights. It seemed to do better with 
extremely heavy CORNER_WEIGHT values, to the point where it effectively grabbed 
corners whenever possible. If I remember correctly, one configuration that 
heavily penalized moves that allowed an opponent to move into a corner or edge 
while only lightly favoring corners and edges for yourself interestingly 
resulted in a strategy that beat ConstantTimePlayer consistently when 
playing as one color, but lost to ConstantTimePlayer consistently when playing 
as the other color.

I found that the best strategy was to weigh the corners and edges such that 
edge squares were always favored against non-edge squares, and corners
were always favored against edges. Meanwhile, the penalty for allowing 
an opponent to move into a corner was much greater than the penalty for allowing 
an opponent to move into an edge. The priority list was then:

Corner >> Edges >> "Neutral Moves" >> Allowed Edge for Opponent >> Allowed Corner for Opponent

Ultimately, I could not beat ConstantTimePlayer as both sides more than about 
60% of the time, which led me to use a slightly-more complex system of weights...

==================
- FINAL STRATEGY -
==================

STRATEGY:

I looked up different weighting strategies online, and tweaked them a bit. Each 
square is now weighted as follows:

         {320, 0.2,  40,   5,   5,  40, 0.2, 320}
         {0.2, 0.1, 0.5, 0.5, 0.5, 0.5, 0.1, 0.2}
         { 40, 0.5,  15,   1,   1,  15, 0.5,  40}
         {  5, 0.5,   1,   1,   1,   1, 0.5,   5}
         {  5, 0.5,   1,   1,   1,   3, 0.5,   5}
         { 40, 0.5,  15,   1,   1,  15, 0.5,  40}
         {0.2, 0.1, 0.5, 0.5, 0.5, 0.5, 0.1, 0.2}
         {320, 0.2,  40,   5,   5,  40, 0.2, 320}

- Favor corners A LOT
- Favor edges unless they allow an opponent to move into a corner
- Favor moves that are two spaces away (and, hence, possibly two moves away) 
  from a corner
- Moderately penalize moves that allow an opponent to move into a corner
- Lightly penalize moves that allow an opponent to move into an edge

RESULTS:

This strategy beats ConstantTimePlayer consistently. It takes into account 
more scenarios than the simplistic weights used in #3. The difference is now 
that squares that have a greater chance of allowing the player to grab a corner 
on the *next* move are also weighted more, and squares on the edge that may 
allow an opponent to take a corner on the opponent's next turn are also 
penalized.

EXPECTATIONS FOR THE TOURNAMENT:

Systems that solely use weights like mine are extremely rudimentary ways of trying 
to guess the opponent's next move using only probabilities based on 
naive games. I fully expect to be trounced by AI's that use decision trees or 
a more complex system of weights. My guess is that AI's that can quickly compute 
deep decision trees will do the best, followed by AI's that combine shallow
decision trees with weights, then AI's that use scenario-dependent weights 
(e.g., weights for a certain square will change depending on the distribution 
of pieces on the board), then AI's that use individual weights like mine, and 
finally AI's that only weight corners and edges.

Please note: I actually tried combining weights with decision tree (2-deep) and 
it performed worse than just using the weights I currently have, so I'm just 
going with the weights. If I spent some time tweaking the weights, I might be 
able to achieve better results, but that may take a while. Refined weights 
guess at the expected outcomes of certain moves well enough, compared to 
a shallow decision tree with no-so-great weights...

Also, when playing BetterPlayer, whoever is black wins.
