#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"

using namespace std;

class Player {

public:
    Player(Side givenSide);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *minimax();

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    
    Side side;
    Board currentBoard;
};

#endif
