/*
 * @filename player_backup.cpp
 */

#include "player.h"

#define INVALID -9999

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side givenSide) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     
     currentBoard = Board();
     side = givenSide;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's move before calculating your own move
     */ 
    
    /* Store the next board. */
    Board *nextBoard;
    
    /* Store the side. */
    Side try_side;
    
    /* Process the opponent's move */
    try_side = (side == BLACK) ? WHITE : BLACK;
    currentBoard.doMove(opponentsMove, try_side);
    
    /* If we have no moves left, return NULL. Otherwise, do a move. */
    if (!currentBoard.hasMoves(side)) {
        return NULL;
    } else {
        
        // Initialize the move
        Move *try_move = new Move(0, 0);
        try_side = side;
        
        // Iterate through all valid moves. Score and store the valid moves.
        int coords[8][8];
        for (int i = 0; i < 8; i++) {
            try_move->setY(i);
            for (int j = 0; j < 8; j++) {
                nextBoard = currentBoard.copy();
                try_move->setX(j);
                if (currentBoard.checkMove(try_move, try_side)) {
                    nextBoard->doMove(try_move, try_side);
                    coords[i][j] = nextBoard->count(try_side);
                } else {
                    coords[i][j] = INVALID;
                }
                delete nextBoard;   
            }
        }
        
        /* Find the best valid move. */ 
        
        int high_i = 0;
        int high_j = 0;
        int high_score = 0;
        
        // Find the highest-scoring corner, if possible.
        bool corner_taken = false;
        if (coords[0][0] != INVALID) {
            high_score = coords[0][0];
            corner_taken = true;
        }
        if ((coords[0][8 - 1] != INVALID) && (coords[0][8 - 1] >= high_score)) {
            high_i = 0;
            high_j = 8 - 1;
            high_score = coords[0][8 - 1];
            corner_taken = true;
        }
        if ((coords[8 - 1][0] != INVALID) && (coords[8 - 1][0] >= high_score)) {
            high_i = 8 - 1;
            high_j = 0;
            high_score = coords[8 - 1][0];
            corner_taken = true;
        }
        if ((coords[8 - 1][8 - 1] != INVALID) && (coords[8 - 1][8 - 1] >= high_score)) {
            high_i = 8 - 1;
            high_j = 8 - 1;
            high_score = coords[8 - 1][8 - 1];
            corner_taken = true;
        }
        
        // Find the highest-scoring non-corner move, if applicable
        if (!corner_taken) {
            high_i = 0;
            high_j = 0;
            high_score = 0;
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if ((coords[i][j] != INVALID) && (coords[i][j] >= high_score)) {
                        high_i = i;
                        high_j = j;
                        high_score = coords[i][j];
                    }
                }  
            }
        }
        
        // Set the highest-scoring move
        try_move->setY(high_i);
        try_move->setX(high_j);
        
        // Apply the move to the current board
        currentBoard.doMove(try_move, side);
        
        // Return the highest-scoring move
        return try_move; 
        
    }

}
